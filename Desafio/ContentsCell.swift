//
//  ContentsCell.swift
//  Desafio
//
//  Created by James Hoffman on 10/30/15.
//  Copyright © 2015 Trinca. All rights reserved.
//

import UIKit

class ContentsCell : UITableViewCell {
    
    @IBOutlet weak var contentsView: UIView!
    @IBOutlet weak var contentTitleLabel: UILabel!
    @IBOutlet weak var contentsDescriptionLabel: UILabel!
    @IBOutlet weak var contentDurationLabel: UILabel!
}