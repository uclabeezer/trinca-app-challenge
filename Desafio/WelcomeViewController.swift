//
//  WelcomeViewController.swift
//  Desafio
//
//  Created by James Hoffman on 10/30/15.
//  Copyright © 2015 Trinca. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    
    // View Objects (from top to bottom, left to right, and bottom layer to top layer)
    @IBOutlet weak var trincaLogoImageView: UIView!
    @IBOutlet weak var trincaLogoImage: UIImageView!
    @IBOutlet weak var cadastrarButton: UIButton!
    
    // MARK: - Initialization Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        configureView()
        // Animate Trinca logo
        heartbeatImageSizeAnimation(trincaLogoImage, imageSizeMultiplier: 1.45, heartRateBPM: 80)
    }
    
    func configureView() {
        // Add background gradient
        let background = CAGradientLayer().blackGradient()
        background.frame = self.view.bounds
        self.view.layer.insertSublayer(background, atIndex: 0)
        
        // Format cadastrar button
        cadastrarButton.layer.borderWidth = 1.2
        cadastrarButton.layer.borderColor = UIColor.whiteColor().CGColor
        cadastrarButton.layer.cornerRadius = 3
        
        cadastrarButton.layer.shadowColor = UIColor.blackColor().CGColor
        cadastrarButton.layer.shadowOffset = CGSizeMake(5, 5)
        cadastrarButton.layer.shadowRadius = 5
        cadastrarButton.layer.shadowOpacity = 1.0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - TapGesture Functions
    
    // Transition to RegisterView
    @IBAction func cadastrarButtonClicked(sender: AnyObject) {
        // Animate Trinca logo and button before segue
        imageExplosionImageSizeAnimation(trincaLogoImage, imageSizeMultiplier: 15)
        cadastrarButton.hidden = true
        
        // Add slight delay to allow for animation before segue
        let delay = 0.1 * Double(NSEC_PER_SEC)
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
        // Perform segue on main queue
        dispatch_after(time, dispatch_get_main_queue()) {
            self.performSegueWithIdentifier("segueToRegisterView", sender: sender)
        }
    }
    
    // MARK: - Animation Functions
    // TODO: Create animation delegate class

    // Animates image like a heartbeat
    // view: UIImageView to be animated
    // imageSizeMultiplier: Magnitude of image expansion during animation
    // heartRateBPM: Rate of imageAnimation in units of heart BPM (normal range: 60 to 100 BPM)
    func heartbeatImageSizeAnimation(view: UIImageView, imageSizeMultiplier: CGFloat, heartRateBPM: Double) {
        // Define animation image sizes
        let normalFrame = CGRect(x: view.frame.origin.x,
                                 y: view.frame.origin.y,
                             width: view.frame.width,
                            height: view.frame.height)
        
        let largerFrame = CGRect(x: view.frame.origin.x-(view.frame.width*(imageSizeMultiplier-1))/2,
                                 y: view.frame.origin.y-(view.frame.height*(imageSizeMultiplier-1))/2,
                             width: view.frame.width*imageSizeMultiplier,
                            height: view.frame.height*imageSizeMultiplier)
        // Set animation rate
        let animationDuration = 60/heartRateBPM
        
        UIView.animateKeyframesWithDuration(animationDuration, delay: 0.5, options: .Repeat, animations: {
                // Animate from normalFrame to largeFrame
                UIView.addKeyframeWithRelativeStartTime(0, relativeDuration: 0.33, animations:{
                        view.frame = largerFrame
                    })
                // Animate from largeFrame to normalFrame
                UIView.addKeyframeWithRelativeStartTime(animationDuration * 1/3, relativeDuration: 0.33, animations:{
                    view.frame = normalFrame
                })
                // Pause animation for heartbeat effect
                UIView.addKeyframeWithRelativeStartTime(animationDuration * 2/3, relativeDuration: 0.34, animations:{
                })
        }, completion: nil)
    }
    
    // Animates image like its exploding
    // view: UIImageView to be animated
    // imageSizeMultiplier: Magnitude of image expansion during animation
    func imageExplosionImageSizeAnimation(view: UIImageView, imageSizeMultiplier: CGFloat) {
        
        let largerFrame = CGRect(x: view.frame.origin.x-(view.frame.width*(imageSizeMultiplier-1))/4,
            y: view.frame.origin.y-(view.frame.height*(imageSizeMultiplier-1))/4,
            width: view.frame.width*imageSizeMultiplier,
            height: view.frame.height*imageSizeMultiplier)
        
        UIView.animateKeyframesWithDuration(0.35, delay: 0, options: [], animations: {
            
            UIView.addKeyframeWithRelativeStartTime(0, relativeDuration: 1, animations:{
                view.frame = largerFrame
            })
            
        }, completion: nil)
    }
}
