//
//  UITextFieldWithPadding.swift
//  Desafio
//
//  Created by James Hoffman on 11/2/15.
//  Copyright © 2015 Trinca. All rights reserved.
//

import UIKit

// Subclassed from UITextField to insert padding before text
class UITextFieldWithPadding: UITextField {
    // Create padding
    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5);

    // Apply padding to each text field aspect
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    override func placeholderRectForBounds(bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    
    // Returns new padded CGRect
    private func newBounds(bounds: CGRect) -> CGRect {
        var newBounds = bounds
        newBounds.origin.x += padding.left
        newBounds.origin.y += padding.top
        newBounds.size.height -= padding.top + padding.bottom
        newBounds.size.width -= padding.left + padding.right
        return newBounds
    }
}
