//
//  HomeViewTabBarController.swift
//  Desafio
//
//  Created by James Hoffman on 10/31/15.
//  Copyright © 2015 Trinca. All rights reserved.
//

import UIKit

// Subclassed from UITabBarController to allow tabBarController customization
class HomeViewTabController : UITabBarController {
    
    // MARK: - Initialization Functions
    
    override func viewDidLoad() {
        // Make the Pauta tab the default selected tab
        self.selectedIndex = 1
        
        configureView()
    }
    
    func configureView() {
        // Change background color for tabBar item icons
        self.tabBar.tintColor = UIColor.whiteColor()
        
        // Add top border to tabBar
        // Create a new layer which is the width of the device and with a height 1 pixel
        let topBorder = CALayer()
        topBorder.frame = CGRectMake(0, 0, self.view.frame.size.width, 1)
        topBorder.backgroundColor = UIColor.whiteColor().CGColor
        // Add the layer to the tab bar's existing layer
        self.tabBar.layer.addSublayer(topBorder)
        // Hide the default system border
        self.tabBar.clipsToBounds = true
        
        // Format tabBar item fonts and positions
        let titleFont : UIFont = UIFont(name: "Nexa Book", size: 10.0)!
        let attributes = [
            NSFontAttributeName : titleFont
        ]
        // Ponto tab
        self.tabBar.items?[0].setTitleTextAttributes(attributes, forState: .Normal)
        self.tabBar.items?[0].titlePositionAdjustment = UIOffsetMake(20,-5)
        // Pauta tab
        self.tabBar.items?[1].setTitleTextAttributes(attributes, forState: .Normal)
        self.tabBar.items?[1].titlePositionAdjustment = UIOffsetMake(0,-5)
        // Jobs tab
        self.tabBar.items?[2].setTitleTextAttributes(attributes, forState: .Normal)
        self.tabBar.items?[2].titlePositionAdjustment = UIOffsetMake(-20,-5)
    }

}