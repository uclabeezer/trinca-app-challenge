//
//  UIImageExtension.swift
//  Desafio
//
//  Created by James Hoffman on 11/4/15.
//  Copyright © 2015 Trinca. All rights reserved.
//

import UIKit

// Extended to make sure UIImages are properly displayed
extension UIImage {
    
    // Pictures captured in camera sessions are marked as portrait
    // This method aligns their content with the portait orientation
    func rotateImageToPortraitOrientation(deviceOrientation : UIDeviceOrientation) -> UIImage {
        let rotatedImage : UIImage?
        
        switch (deviceOrientation) {
        case .Portrait:
            rotatedImage = self.rotateImageBy(180)
            break
        case .LandscapeRight:
            rotatedImage = self.rotateImageBy(-90)
            break;
        case .PortraitUpsideDown:
            rotatedImage = self.rotateImageBy(0)
            break
        
        default:
            rotatedImage = self.rotateImageBy(180)
            break
        }
        
        return rotatedImage!
    }
    
    // TODO: Review usage of these non-thread safe functions
    
    // Photos are sometimes stored out of their native orientation by iOS to save memory
    // This method orients displayed images
    func nativelyOrientedImage() -> UIImage {
        if self.imageOrientation == UIImageOrientation.Up {
            return self
        }
        
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.drawInRect(CGRectMake(0, 0, self.size.width, self.size.height))
        let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return normalizedImage
    }
    
    // Clips image so it fits perfectly in square imageView that uses ScaleAspectFit
    func squareClippedImage() -> UIImage {
        let height = self.size.height
        let width = self.size.width
        let squareSideLength : CGFloat?
        let clipHeight : Bool
        let clipAdjustment : CGFloat
        
        if height == width {
            return self
        }
        
        if height > width {
            squareSideLength = width
            clipHeight = true
            clipAdjustment = (height - width)/2
        } else {
            squareSideLength = height
            clipHeight = false
            clipAdjustment = (width - height)/2
        }
        
        let squareSize = CGSize(width: squareSideLength!, height: squareSideLength!)
        
        UIGraphicsBeginImageContext(squareSize)
            if clipHeight {
                 self.drawInRect(CGRectMake(0, -clipAdjustment, self.size.width, self.size.height))
            } else {
                self.drawInRect(CGRectMake(-clipAdjustment, 0, self.size.width, self.size.height))
            }
            let squareImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return squareImage
    }

    // Rotates images by degrees
    private func rotateImageBy(degrees: CGFloat) -> UIImage {
        let degreesToRadians: (CGFloat) -> CGFloat = {
            return $0 / 180.0 * CGFloat(M_PI)
        }
        
        // calculate the size of the rotated view's containing box for our drawing space
        let rotatedViewBox = UIView(frame: CGRect(origin: CGPointZero, size: size))
        let t = CGAffineTransformMakeRotation(degreesToRadians(degrees));
        rotatedViewBox.transform = t
        let rotatedSize = rotatedViewBox.frame.size
        
        // Create the bitmap context
        UIGraphicsBeginImageContext(rotatedSize)
        let context = UIGraphicsGetCurrentContext()
        
        // Move the origin to the middle of the image so we will rotate and scale around the center.
        CGContextTranslateCTM(context, rotatedSize.width / 2.0, rotatedSize.height / 2.0);
        
        //   // Rotate the image context
        CGContextRotateCTM(context, degreesToRadians(degrees));
        CGContextDrawImage(context, CGRectMake(-size.width / 2, -size.height / 2, size.width, size.height), CGImage)
        
        let rotatedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return rotatedImage
    }
}