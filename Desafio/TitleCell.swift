//
//  TitleCell.swift
//  Desafio
//
//  Created by James Hoffman on 11/3/15.
//  Copyright © 2015 Trinca. All rights reserved.
//

import UIKit

class TitleCell : UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
}