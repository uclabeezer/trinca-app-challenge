//
//  RegisterViewController.swift
//  Desafio
//
//  Created by James Hoffman on 10/30/15.
//  Copyright © 2015 Trinca. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class RegisterViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    // View Objects (from top to bottom, left to right, and bottom layer to top layer)
    @IBOutlet var masterUIView: UIView!
    @IBOutlet weak var escolherAvatarButton: UIButton!
    @IBOutlet weak var escolherAvatarTextView: UITextView!
    @IBOutlet weak var textFieldsViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var nomeTextField: UITextFieldWithPadding!
    @IBOutlet weak var sexoTextField: UITextFieldWithPadding!
    @IBOutlet weak var idadeTextField: UITextFieldWithPadding!
    @IBOutlet weak var cargoTextField: UITextFieldWithPadding!
    @IBOutlet weak var enviarButtonView: UIView!
    @IBOutlet weak var enviarButton: UIButton!
    var uiPicker: UIPickerView?  // uiPicker for sexoTextField
    
    // Locals
    var avatar : UIImage?
    var numberPadHeight :CGFloat?
    
    // Camera setup variables
    let captureSession = AVCaptureSession()
    var previewLayer : AVCaptureVideoPreviewLayer?
    let stillImageOutput = AVCaptureStillImageOutput()
    // If we find a device we'll store it here for later use
    var captureDevice : AVCaptureDevice?
    var captureDevices : [String] = []
    
    // MARK: - Initialization Functions
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        
        // Set RegisterViewController as delegate for textFields
        nomeTextField.delegate = self
        sexoTextField.delegate = self
        idadeTextField.delegate = self
        cargoTextField.delegate = self
        
        // Create NSNotifications for KeyboardWillShow and KeyboardWillHide events
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
    }
    
    func configureView(){
        // Add background gradient
        let background = CAGradientLayer().blackGradient()
        background.frame = self.view.bounds
        self.view.layer.insertSublayer(background, atIndex: 0)
        
        // Format escolherAvatarButton
        escolherAvatarButton.layer.cornerRadius = 43.6
        escolherAvatarButton.clipsToBounds = true
        escolherAvatarButton.layer.borderWidth = 2
        escolherAvatarButton.layer.borderColor = UIColor.whiteColor().CGColor
        escolherAvatarTextView.editable = false
        escolherAvatarButton.imageView?.contentMode = .ScaleAspectFit
        
        // Format text fields
        let yellowColor = UIColor(red: 246.0/255, green: 255.0/255, blue: 0.0/255, alpha: 1)
        
        // nomeTextField
        setAttributedPlaceholderIn(nomeTextField, text: "Nome Completo*", color: yellowColor)
        addUnderlineTo(nomeTextField, borderWidth: 1.8, color:UIColor.whiteColor())
        // sexoTextField
        setAttributedPlaceholderIn(sexoTextField, text: "Sexo", color: yellowColor)
        addUnderlineTo(sexoTextField, borderWidth: 1.8, color:UIColor.whiteColor())
        // Add downArrow image to sexoTextField
        let imageView = UIImageView(frame:CGRectMake(sexoTextField.frame.width*0.93,
                                                     sexoTextField.frame.height*0.2,
                                                     sexoTextField.frame.width*0.04,
                                                     sexoTextField.frame.height*0.6))
        imageView.image = UIImage(named: "downarrow_vector.pdf")
        imageView.contentMode = .ScaleToFill
        sexoTextField.addSubview(imageView)
        // idadeTextField
        setAttributedPlaceholderIn(idadeTextField, text: "Idade", color: yellowColor)
        addUnderlineTo(idadeTextField, borderWidth: 1.8, color:UIColor.whiteColor())
        addDoneButtonToKeyboardOf(idadeTextField)
        // cargoTextField
        setAttributedPlaceholderIn(cargoTextField, text: "Cargo*", color: yellowColor)
        addUnderlineTo(cargoTextField,borderWidth: 1.8, color:UIColor.whiteColor())
        
        // Create UIPicker for sexoTextField
        uiPicker = UIPickerView(frame: CGRectMake(0,0,masterUIView.frame.width,masterUIView.frame.height/5))
        uiPicker?.delegate = self
        uiPicker?.dataSource = self
        uiPicker?.backgroundColor = UIColor.blackColor()
        sexoTextField.inputView = uiPicker
        // Add Done button to uiPicker
        let uiPickerToolBar = UIToolbar()
        uiPickerToolBar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: "keyboardDoneKeyClicked:")
        uiPickerToolBar.items = [flexBarButton, doneButton]
        uiPickerToolBar.userInteractionEnabled = true
        sexoTextField.inputAccessoryView = uiPickerToolBar
        
        // Format enviarButton
        enviarButton.layer.cornerRadius = 3
        enviarButton.layer.borderWidth = 1.2
        enviarButton.layer.borderColor = UIColor.whiteColor().CGColor

        enviarButton.layer.shadowColor = UIColor.blackColor().CGColor
        enviarButton.layer.shadowOffset = CGSizeMake(5, 5)
        enviarButton.layer.shadowRadius = 5
        enviarButton.layer.shadowOpacity = 1.0
        
        // Set numberPadHeight so textFieldView in RegisterViewController knows where to move when keyboards are displayed
        self.numberPadHeight = getNumberPadHeight()
    }
    
    // Sets textField placeholder value and text color
    func setAttributedPlaceholderIn(textField: UITextField, text: String, color: UIColor) {
        textField.attributedPlaceholder = NSAttributedString(string:text,
            attributes:[NSForegroundColorAttributeName: color])
    }
    
    // Adds underline border to textField
    func addUnderlineTo(textField: UITextField, borderWidth: Double, color: UIColor){
        let border = CALayer()
        let width = CGFloat(borderWidth)
        border.borderColor = color.CGColor
        border.borderWidth = width
        
        border.frame = CGRect(x: 0,
            y: textField.frame.size.height - width,
            width:  textField.frame.size.width,
            height: textField.frame.size.height)
        
        textField.layer.addSublayer(border)
        textField.layer.masksToBounds = true
    }
    
    func addDoneButtonToKeyboardOf(textField: UITextField) {
        // Add done button to keyboard
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: "keyboardDoneKeyClicked:")
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        textField.inputAccessoryView = keyboardToolbar
    }
    
    func getNumberPadHeight() -> CGFloat {
        // Create numberPad with Done button and return its height
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: "keyboardDoneKeyClicked:")
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        return keyboardToolbar.frame.height
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - NSNotification Functions
    
    // Animates textFieldsView, escolherAvatarButton, and escolherAvatarTextView when keyboardWillShow
    func keyboardWillShow(notification: NSNotification) {
        // Ensure numberPadHeight has a value
        guard let numberPadHeight = self.numberPadHeight else {
            return
        }
        // Ensure that animation is really necessary (this prevents text field flicker)
        guard (self.textFieldsViewBottomConstraint.constant != numberPadHeight + 80) else {
            return
        }
        
        // Set object constraints for animation
        self.textFieldsViewBottomConstraint.constant = numberPadHeight + 80
        
        UIView.animateWithDuration(2, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
        
        // Hide items as keyboard rises
        self.escolherAvatarButton.hidden = true
        self.escolherAvatarTextView.hidden = true
    }
    
    // Animates textFieldsView, escolherAvatarButton, and escolherAvatarTextView when when keyboardWillHide
    func keyboardWillHide(notification: NSNotification) {
        // Ensure numberPadHeight has a value
        guard let numberPadHeight = self.numberPadHeight else {
            return
        }
        // Ensure that animation is really necessary (this prevents text field flicker)
        guard (self.textFieldsViewBottomConstraint.constant == numberPadHeight + 80) else {
            return
        }
        
        // Set object constraints for animation
        self.textFieldsViewBottomConstraint.constant = 0

        // Ensure avatar image is nil before showing escolherAvatarTextView
        if self.escolherAvatarButton?.imageView?.image == nil {
            self.escolherAvatarTextView.hidden = false
        }

        UIView.animateWithDuration(2, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
        
        // Reappear items as keyboard falls
        self.escolherAvatarButton.hidden = false
    }

    // MARK: - PrepareForSegue
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueToHomeView" {
            let referenceToHomeView : UITabBarController = segue.destinationViewController as! UITabBarController
            let referenceToSecondTabbedView = referenceToHomeView.viewControllers?[1] as! SecondTabbedViewController
            // Set avatar image value on SecondTabbedViewController
            referenceToSecondTabbedView.avatar = escolherAvatarButton?.imageView?.image?.squareClippedImage()
        }
    }
    
    // MARK: Camera Functions
    // TODO: Create camera delegate class
    
    func startCameraSession() {
        
        // configureDevice()
        
        // Capture Device
        do {
            // Ensure sure device is not added to captureSession more than once
            if captureDevices.contains((captureDevice?.localizedName)!) == false {
                try captureSession.addInput(AVCaptureDeviceInput(device: captureDevice))
                captureDevices.append((captureDevice?.localizedName)!)
                print(captureDevice?.localizedName)
            }
        } catch {
            print("Error capturing device")
            print("error: \(error)")
        }
        
        captureSession.sessionPreset = AVCaptureSessionPresetPhoto
        captureSession.startRunning()
        stillImageOutput.outputSettings = [AVVideoCodecKey:AVVideoCodecJPEG]
        if captureSession.canAddOutput(stillImageOutput) {
            captureSession.addOutput(stillImageOutput)
        }

        if let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession) {
            previewLayer.bounds = CGRectMake(0.0, 0.0, view.bounds.size.width, view.bounds.size.height)
            previewLayer.position = CGPointMake(view.bounds.midX, view.bounds.midY)
            previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
            let cameraPreview = UIView(frame: CGRectMake(0.0, 0.0, view.bounds.size.width, view.bounds.size.height))
            cameraPreview.layer.addSublayer(previewLayer)
            cameraPreview.addGestureRecognizer(UITapGestureRecognizer(target: self, action:"takePicture:"))
            // Set tag to reference cameraPreview subview
            cameraPreview.tag = 100
            view.addSubview(cameraPreview)
        }
    }
    
    
    // This is a TapGesture Function but its grouped here with the other Camera Functions
    // Function to respond when user clicks cameraPreview View
    func takePicture(sender: UITapGestureRecognizer) {
        // Ensure tapGestureRecognizer is only triggered in cameraView
        guard self.view.viewWithTag(100) != nil else {
            return
        }
        
        let deviceOrientation = UIDevice.currentDevice().orientation
        
        if let videoConnection = stillImageOutput.connectionWithMediaType(AVMediaTypeVideo) {
            stillImageOutput.captureStillImageAsynchronouslyFromConnection(videoConnection) {
                (imageDataSampleBuffer, error) -> Void in
                
                let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)
                // Save to Camera, Possible feature to add
                //UIImageWriteToSavedPhotosAlbum(UIImage(data: imageData)!, nil, nil, nil)
                
                if let avatar : UIImage = UIImage(data: imageData) {

                    let avatarSquare = avatar.squareClippedImage().nativelyOrientedImage()
                    self.escolherAvatarButton?.setImage(avatarSquare.rotateImageToPortraitOrientation(deviceOrientation), forState: .Normal)
                }
                
                // Hide other escolherAvatarButton elements as not to obscure image
                self.escolherAvatarTextView.hidden = true
                self.escolherAvatarButton.layer.borderColor = UIColor.clearColor().CGColor
            }
        }
        // Dismiss cameraPreview
        if let viewWithTag = self.view.viewWithTag(100) {
            viewWithTag.removeFromSuperview()
        }
        else {
            print("tag not found")
        }
    }
    
    // This is a Delegate Function but its grouped here with the other Camera Functions
    // Function to respond to when user selects image
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let avatar : UIImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            escolherAvatarButton?.setImage(avatar.squareClippedImage(), forState: .Normal)
        }

        // Hide other escolherAvatarButton elementa as not to obscure image
        self.escolherAvatarTextView.hidden = true
        self.escolherAvatarButton.layer.borderColor = UIColor.clearColor().CGColor
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    // MARK: - TapGesture Functions
    
    // Use camera to get image if available, uses photo library if camera is not available
    @IBAction func escolherAvatarButtonClicked(sender: AnyObject) {
        
        let devices = AVCaptureDevice.devices()
        
        // Display photoLibrary if no camera is available
        guard devices.isEmpty == false else {
            let imageFromSource = UIImagePickerController()
            imageFromSource.delegate = self
            imageFromSource.allowsEditing = false
            imageFromSource.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            
            self.presentViewController(imageFromSource, animated: true, completion: nil)
            return
        }
        
        // Loop through all the capture devices on this phone
        for device in devices {
            // Make sure this particular device supports video
            if (device.hasMediaType(AVMediaTypeVideo)) {
                // Finally check the position and confirm we've got the back camera
                if(device.position == AVCaptureDevicePosition.Front) {
                    captureDevice = device as? AVCaptureDevice
                    if captureDevice != nil {
                        print("Capture device found")
                        startCameraSession()
                    }
                }
            }
        }
    }
    
    // Transition to HomeView
    @IBAction func enviarButtonClicked(sender: AnyObject) {
        // Ensure required field values have been entered
        // If not, format required fields to notify user
        guard nomeTextField.text != "" &&
            cargoTextField.text != "" else {
                if nomeTextField.text == "" {
                    nomeTextField.attributedPlaceholder = NSAttributedString(
                        string:"Nome Completo*",
                        attributes:[NSForegroundColorAttributeName:
                        UIColor(red: 246.0/255, green: 72.0/255, blue: 46.0/255, alpha: 1)]
                    )
                }
                if cargoTextField.text == "" {
                    cargoTextField.attributedPlaceholder = NSAttributedString(
                        string:"Cargo*",
                        attributes:[NSForegroundColorAttributeName:
                        UIColor(red: 246.0/255, green: 72.0/255, blue: 46.0/255, alpha: 1)]
                    )
                }
                return
        }
        performSegueWithIdentifier("segueToHomeView", sender: sender)
    }
    
    // Notify textFields they are done being edited when user clicks Done
    // This triggers textFieldShouldReturn() which resigns the keyboard
    func keyboardDoneKeyClicked(sender: AnyObject?) {
        nomeTextField.endEditing(true)
        sexoTextField.endEditing(true)
        idadeTextField.endEditing(true)
        cargoTextField.endEditing(true)
    }
    
    // MARK: Delegate Functions
    
    // Prevents user from entered invalid characters
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        // Make sexoTextField uneditable
        if textField == sexoTextField {
            return false
        }
        
        // Only numbers can be entered in idadeTextField
        if textField == idadeTextField {
            // Find out what the text field will be after adding the current edit
            // Get current text
            let text = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
            
            // Allow text value to be an empty string
            if text == "" {
                return true
            }
            
            // Ensure text field successfully converted to an Int
            guard let _ = Int(text) else {
                return false
            }
        }
        // Return true so the text field will be changed
        return true
    }
    
    // Resigns keyboard when field is done editing field
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    // The following pickerView functions set the dimensions and contents of UIPicker
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // Returns number of rows in each component
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 3
    }
    
    // Returns rowHeight for each row
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 18
    }
    
    // Returns each formatted item to display
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var string : String?
        // Set sexoTextField uiPicker values
        if row == 0 {
            string = ""
        } else if row == 1 {
            string = "Masculino"
        } else if row == 2 {
            string = "Feminino"
        }
        // Format and return
        return NSAttributedString(
                    string: string!,
                    attributes: [NSForegroundColorAttributeName:
                    UIColor(red: 246.0/255, green: 255.0/255, blue: 0.0/255, alpha: 1)]
        )
    }
    
    // Retrieves the current selection when touched
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row == 0 {
            sexoTextField.text = "Sexo"
        } else if row == 1 {
            sexoTextField.text = "Masculino"
        } else{
            sexoTextField.text = "Feminino"
        }
        
        return
    }
}