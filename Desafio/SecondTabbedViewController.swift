//
//  SecondViewController.swift
//  Desafio
//
//  Created by James Hoffman on 10/30/15.
//  Copyright © 2015 Trinca. All rights reserved.
//

import UIKit

// MARK: - DaysOfTheWeek Enum

// Enum to retrieve weekdayString from weekdayInt
enum DaysOfTheWeek: Int {
    case Domingo = 1, Segunda, Terça, Quarta, Quinta, Sexta, Sabado
    
    // This internal enum function returns the dayName
    func associatedDayName() -> String? {
        var dayName : String?
        
        switch self {
        case .Domingo:
            dayName = "Domingo"
        case .Segunda:
            dayName = "Segunda"
        case .Terça:
            dayName = "Terça"
        case .Quarta:
            dayName = "Quarta"
        case .Quinta:
            dayName = "Quinta"
        case .Sexta:
            dayName = "Sexta"
        case .Sabado:
            dayName = "Sabado"
        }
        return dayName
    }
}

// MARK: - SecondTabbedViewController Class

class SecondTabbedViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // View Objects (from top to bottom, left to right, and bottom layer to top layer)
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var todaysDateLabel: UILabel!
    @IBOutlet weak var contentsTableView: UITableView!
    
    // Locals
    var avatar : UIImage?  // Set by RegisterViewController
    
    // Sample data
    let contentArrayOfDict :[[String:AnyObject]] = [
        ["cellType": 0,"title":"Manhã","time":"","description":""],
        ["cellType": 1,"title":"Protector","time":2,"description":"Integrar base de dados"],
        ["cellType": 1,"title":"Red Bull","time":1,"description":"Redirecionar site"],
        ["cellType": 0,"title":"Tarde","time":"","description":""],
        ["cellType": 1,"title":"FitSkin","time":4,"description":"Enviar e-mails automáticos"],
        ["cellType": 1,"title":"FTM","time":1,"description":"Não posso ver ele"],
        ["cellType": 0,"title":"Noite","time":"","description":""],
        ["cellType": 1,"title":"Hire James","time":1,"description":"A gente deve contrate-me :)"],
        ["cellType": 1,"title":"Churrasco","time":5,"description":"Bebemos cervezas"]
    ]
    
    // MARK: - Initialization Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        
        // contentsTableView is reloaded to display cell heights properly
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.contentsTableView.reloadData()
        })
    }
    
    func configureView() {
        // Add background gradient
        let background = CAGradientLayer().blackGradient()
        background.frame = self.view.bounds
        self.view.layer.insertSublayer(background, atIndex: 0)
        
        // Format avatarImageView
        avatarImageView.image = avatar
        avatarImageView.layer.cornerRadius = 25
        avatarImageView.clipsToBounds = true
        avatarImageView.contentMode = .ScaleAspectFit
        
        // Get today's date and apply to todaysDateLabel
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Weekday,.Day,.Month], fromDate: date)
        let month = components.month
        let day = components.day
        let weekdayInt = components.weekday
        let daysOfTheWeekEnum = DaysOfTheWeek(rawValue: weekdayInt)
        // Ensure weekdayString is properly set
        if let weekdayString = daysOfTheWeekEnum?.associatedDayName() {
            todaysDateLabel.text = "\(weekdayString) • \(day)" + "/" + "\(month)"
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Delegate Functions
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    // Creates as many rows in contentsTableView as there are items in contentsArrayOfDict
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentArrayOfDict.count
    }
    
    // Creates table cells
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Ensure cellType is valid
        guard let cellType = contentArrayOfDict[indexPath.row]["cellType"] as? Int else{
            print("Invalid cellType encountered in contentsTableView.")
            let cell = tableView.dequeueReusableCellWithIdentifier("cellID", forIndexPath: indexPath)
            return cell
        }
       
        // TitleCell
        if cellType == 0 {
            // Set row height
            contentsTableView.rowHeight = 30.5
            // Create cell
            let cell0 = tableView.dequeueReusableCellWithIdentifier("titleCellID", forIndexPath: indexPath) as! TitleCell
            
            // Ensure title can be unwrapped
            guard let title = contentArrayOfDict[indexPath.row]["title"] as? String else {
                return cell0
            }
            
            // Set displayed values and return cell
            cell0.titleLabel.text = title
            return cell0
        // ContentsCell
        } else if cellType == 1 {
            // Automatically calculate row height based on contents
            contentsTableView.estimatedRowHeight = 100
            contentsTableView.rowHeight = UITableViewAutomaticDimension
            // Create cell
            let cell1 = tableView.dequeueReusableCellWithIdentifier("contentsCellID", forIndexPath: indexPath) as! ContentsCell
            
            // Ensure title, description, and time can be unwrapped
            guard let title = contentArrayOfDict[indexPath.row]["title"] as? String,
                description = contentArrayOfDict[indexPath.row]["description"] as? String,
                       time = contentArrayOfDict[indexPath.row]["time"] as? Int else {
                return cell1
            }
            
            // Set displayed values and return cell
            cell1.contentTitleLabel.text = title
            cell1.contentsDescriptionLabel.text = description
            cell1.contentDurationLabel.text = "\(time)h"
            
            // Format cell border
            cell1.contentsView.layer.borderWidth = 0.8
            cell1.contentsView.layer.borderColor = UIColor.whiteColor().CGColor
            return cell1
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("cellID", forIndexPath: indexPath)
            return cell
        }

    }
    
    // TODO: Resolve Warning
    // The following section is included to resolve "Warning once only: Detected a case where constraints ambiguously suggest a height of zero for a tableview cell's content view."  Warning is thrown after last cell is created in table.
    
//    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        guard let cellType = contentDict[indexPath.row]["cellType"] as? Int else{
//            return 44
//        }
//        
//        if cellType == 0 {
//            return 30.5
//        } else {
//            return 100
//        }
//        
//    }
}

