//
//  HomeViewTabBar.swift
//  Desafio
//
//  Created by James Hoffman on 10/31/15.
//  Copyright © 2015 Trinca. All rights reserved.
//

import UIKit

// Subclassed from UITabBar to enable tabBar resizing
class HomeViewTabBar: UITabBar {
    // Adjust height of tabBar
    override func sizeThatFits(size: CGSize) -> CGSize {
        var sizeThatFits = super.sizeThatFits(size)
        
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        sizeThatFits.height = screenSize.height*0.114
        
        return sizeThatFits
    }
}