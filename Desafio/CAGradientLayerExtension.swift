//
//  CAGradientLayerExtension.swift
//  Desafio
//
//  Created by James Hoffman on 10/31/15.
//  Copyright © 2015 Trinca. All rights reserved.
//

import UIKit

// Extended to create blackGradient layer
extension CAGradientLayer {
    // Creates blackGradient layer
    func blackGradient() -> CAGradientLayer {
        
        // Pick gradient colors
        let topColor: UIColor = UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 1)
        let bottomColor: UIColor = UIColor(red: 37.0/255, green: 37.0/255, blue: 37.0/255, alpha: 1)
        
        // Create gradient attributes
        let gradientColors: [CGColor] = [topColor.CGColor, bottomColor.CGColor]
        let gradientLocations: [Float] = [0, 1]
        
        // Create gradient
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientColors
        gradientLayer.locations = gradientLocations
        
        return gradientLayer
    }
}
